import bpy
import json

from time import gmtime, strftime

try:
    traj_object = bpy.data.objects['trajectory']
except Exception as err:
    print("There is no trajectory object on scene.")
    exit(-1)
    
traj_pivot_loc  = traj_object.location
traj_points     = traj_object.data.vertices.values()
traj_points_num = len(traj_points)


cur_blend_file_name = bpy.path.basename(bpy.context.blend_data.filepath)
cur_blend_file_dir  = bpy.context.blend_data.filepath[:-len(cur_blend_file_name)]
traj_file_path      = cur_blend_file_dir + cur_blend_file_name[:-6] + "_traj_" + strftime("%d.%m.%Y %H:%M:%S", gmtime())

try:
    traj_file = open(traj_file_path, 'w')
except Exception as err:
    print("Can't open file " + traj_file_path)
    print(err)
    exit(-1)


traj = {"points":[]}
point_idx = 0
for point in traj_points:
    traj["points"].append({"point_idx": point_idx,
                            "x" : traj_points[point_idx].co[0] + traj_pivot_loc[0],
                            "y" : traj_points[point_idx].co[1] + traj_pivot_loc[1],
                            "z" : traj_points[point_idx].co[2] + traj_pivot_loc[2] })
    point_idx += 1


traj_file.write(json.dumps(traj))
traj_file.flush()
traj_file.close()
print("DONE! File location: " + traj_file_path)